#pragma once

#include <iostream>
#include <string>

using std::string;
using std::ostream;

/**
  Complex number class (C++)
*/
class Complex {
private:
  double real;
  double imaginary;
public:
  /**
    Complex number default constructor
  */
  Complex();
  /**
    Complex number parameterized constructor
  */
  Complex(double real, double imaginary);
  /**
    Returns the real component of the complex number
  */
  double getReal() const;
  /**
    Returns the imaginary component of the complex number
  */
  double getImaginary() const;
  /**
    Sets the real component of the complex number
  */
  void setReal(double newReal);
  /**
    Sets the imaginary component of the complex number
  */
  void setImaginary(double newImaginary);
  /**
    Returns if the current complex number equals the Complex nmber c
  */
  bool equal(const Complex &c) const;
  /**
    Returns the sum of the current complex number and the Complex nmber c
  */
  Complex add(const Complex &c) const;
  /**
    Returns the product of the current complex number and the Complex nmber c
  */
  Complex multiply(const Complex &c) const;
  /**
    Returns the complex number in the form of a + bi
  */
  void output(std::ostream &out) const;
};
