# Automated Documentation Example
An example of setting up Sphinx for C++ and building with CMake and Read the Docs

See the documentation [here](https://complex-documentation.readthedocs.io/en/latest/)
