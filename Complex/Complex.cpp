#include "Complex.h"

Complex::Complex() {
  setReal(1);
  setImaginary(1);
}

Complex::Complex(double real, double imaginary) {
  setReal(real);
  setImaginary(imaginary);
}

double Complex::getReal() const {
  return real;
}

double Complex::getImaginary() const {
  return imaginary;
}

void Complex::setReal(double newReal) {
  real = newReal;
}

void Complex::setImaginary(double newImaginary) {
  imaginary = newImaginary;
}

bool Complex::equal(const Complex &c) const {
  return real == c.getReal() && imaginary == c.getImaginary();
}

Complex Complex::add(const Complex &c) const {
  Complex a;
  a.setReal(real + c.getReal());
  a.setImaginary(imaginary + c.getImaginary());
  return a;
}

Complex Complex::multiply(const Complex &c) const {
  Complex m;
  m.setReal(c.getReal()*real - c.getImaginary()*imaginary);
  m.setImaginary(real*c.getImaginary() + imaginary*c.getReal());
  return m;
}

void Complex::output(std::ostream &out) const {
  out << real << " + " << imaginary << "i" << std::endl;
}
